module libvirt-go-api

go 1.16

require (
	github.com/libvirt/libvirt-go v7.0.0+incompatible
	github.com/libvirt/libvirt-go-xml v7.2.0+incompatible
)
